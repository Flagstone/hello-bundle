# Hello-Bundle

Hello Bundle is my very first bundle to use with Symfony. It is a try/test bundle to try/test possibilities of Symfony Bundle

## Installation

Use composer manager to install it

```bash
composer install farvest/hello
```

## License

[MIT](https://choosealicence.com/licenses/mit/)
