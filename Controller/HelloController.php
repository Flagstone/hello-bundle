<?php


namespace Farvest\HelloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HelloController
 * @package Farvest\HelloBundle\Controller
 * @Route("/hello")
 */
class HelloController extends AbstractController
{
    /**
     * @Route(
     *     name = "hello",
     *     path = "/"
     * )
     */
    public function hello()
    {
        return $this->render('@FarvestHello/pages/hello.html.twig');
    }
}